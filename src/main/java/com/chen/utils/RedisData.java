package com.chen.utils;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author whc
 * @date 2022/9/4 22:15
 */
@Data
public class RedisData {

    private LocalDateTime expireTime;
    private Object data;
}
