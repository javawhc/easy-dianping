package com.chen.utils;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * @author whc
 * @date 2022/9/1 22:03
 */
@Component
@Slf4j
public class CacheClient {


    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    // 线程池
    private static final ExecutorService CACHE_REBUILD_EXECUTOR = Executors.newFixedThreadPool(10);


    /**
     * 设置将任意Java对象序列化为json并存储在string类型的key中，并且可以设置TTL过期时间
     * @param key
     * @param val
     * @param time
     * @param timeUnit
     */
    public void set(String key, Object val, Long time, TimeUnit timeUnit) {
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(val), time, timeUnit);
    }

    /**
     * 将任意Java对象序列化为json并存储在string类型的key中，并且可以设置逻辑过期时间，用于处理缓存问题
     * @param key
     * @param val
     * @param time
     * @param timeUnit
     */
    public void setWithLogicalExpire(String key, Object val, Long time, TimeUnit timeUnit) {
        //设置逻辑过期时间
        RedisData redisData = new RedisData();
        redisData.setData(val);
        redisData.setExpireTime(LocalDateTime.now().plusSeconds(timeUnit.toSeconds(time)));
        //写入redis
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(redisData));
    }

    /**
     * 利用泛型，函数式编程解决查询，完成缓存穿透
     * @param keyPrefix
     * @param id
     * @param type
     * @param dbFallback
     * @param time
     * @param timeUnit
     * @param <R>
     * @param <ID>
     * @return
     */
    public <R, ID> R queryWithPassThrough(
            String keyPrefix, ID id, Class<R> type, Function<ID, R> dbFallback, Long time, TimeUnit timeUnit) {
        String key = keyPrefix + id;
        // 从redis中查询缓存
        String json = stringRedisTemplate.opsForValue().get(key);
        //判断是否存在，存在直接返回即可
        if (StrUtil.isNotBlank(json)) {
            // 解析成对象并返回
            return JSONUtil.toBean(json, type);
        }

        //判断命中的是否是null值
        if (json != null) {
            //返回错误信息
            return null;
        }

        // 不存在，数据库查询
        R r = dbFallback.apply(id);
        if (r == null) {
            //写入redis null 值
            this.set(key, "", time, timeUnit);
            return null;
        }

        //存在，存入redis并返回
        this.set(key, r, time, timeUnit);
        return r;
    }


    /**
     * 利用泛型，函数式编程完成逻辑过期查询，缓存击穿解决
     * @param keyPrefix 前缀
     * @param id    id
     * @param type  指定对象类型
     * @param dbFallBack    函数式编程，调用数据库，里面存放的是业务逻辑  Function<T, R> 其中 T 代表参数，R代表返回结果
     * @param time  过期时间
     * @param timeUnit  过期时间单位
     * @param <R>   返回对象结果
     * @param <ID>  传入参数
     * @return
     */
    public <R, ID> R queryWithLogicalExpire(
            String keyPrefix, ID id, Class<R> type, Function<ID, R> dbFallBack, Long time, TimeUnit timeUnit) {
        String key = keyPrefix + id;
        //1.从redis中查询缓存
        String json = stringRedisTemplate.opsForValue().get(key);
        //2.判断是否存在，不存在返回null
        if (StrUtil.isBlank(json)) {
            return null;
        }

        //3. 命中，把json反序列化为对象
        RedisData redisData = JSONUtil.toBean(json, RedisData.class);
        R r = JSONUtil.toBean((JSONObject) redisData.getData(), type);

        LocalDateTime expireTime = redisData.getExpireTime();

        //4. 判断是否过期
        if (expireTime.isAfter(LocalDateTime.now())) {
            //4.1 如果未过期，直接返回即可
            return r;
        }
        //4.2 如果已过期，需要缓存重建
        //5. 缓存重建
        //5.1 获取互斥锁
        String lockKey = RedisConstants.LOCK_SHOP_KEY + id;
        boolean lock = tryLock(lockKey);
        //5.2 判断是否获取成功
        if (lock) {
            //5.3 成功，开启线程 实现缓存重建
            CACHE_REBUILD_EXECUTOR.submit(() -> {
                try {
                    //重建缓存
                    //查询数据库
                    R r1 = dbFallBack.apply(id);
                    //写入redis
                    this.setWithLogicalExpire(key, r1, time, timeUnit);
                    //释放锁
                } catch (Exception e) {
                    throw new RuntimeException(e);
                } finally {
                    unLock(lockKey);
                }
            });
        }
        //5.4 失败，直接返回结果即可
        return r;
    }

    private boolean tryLock(String key) {
        Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, "1", 10, TimeUnit.SECONDS);
        return BooleanUtil.isTrue(flag);
    }

    private void unLock(String key) {
        stringRedisTemplate.delete(key);
    }

}
