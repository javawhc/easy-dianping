package com.chen.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chen.entity.SeckillVoucher;
import com.chen.mapper.SeckillVoucherMapper;
import com.chen.service.ISeckillVoucherService;
import org.springframework.stereotype.Service;

/**
 * @author whc
 * @date 2022/9/12 13:31
 */
@Service
public class SeckillVoucherServiceImpl extends ServiceImpl<SeckillVoucherMapper, SeckillVoucher> implements ISeckillVoucherService {

}
