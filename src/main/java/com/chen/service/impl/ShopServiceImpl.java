package com.chen.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chen.common.ResultBean;
import com.chen.dto.ShopDTO;
import com.chen.entity.Shop;
import com.chen.mapper.ShopMapper;
import com.chen.service.ShopService;
import com.chen.utils.CacheClient;
import com.chen.utils.RedisConstants;
import com.chen.utils.RedisData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author whc
 * @date 2022/9/3 10:36
 */
@Slf4j
@Service
public class  ShopServiceImpl extends ServiceImpl<ShopMapper, Shop> implements ShopService{

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private CacheClient cacheClient;

    // 线程池
    private static final ExecutorService CACHE_REBUILD_EXECUTOR = Executors.newFixedThreadPool(10);

    @Override
    public ResultBean<ShopDTO> queryById(Long id) {
        try {
            // 解决缓存穿透
//            ShopEntity shopEntity = queryWithPassThrough(id);

//            //解决缓存击穿
//            ShopEntity shopEntity = queryWithLogicalExpire(id);

            // 工具类解决缓存击穿
            Shop shopEntity =
                    cacheClient.queryWithLogicalExpire(RedisConstants.CACHE_SHOP_KEY, 1L, Shop.class, this::getById, 10L, TimeUnit.SECONDS);
            if (shopEntity == null) {
                return ResultBean.create(-1, "店铺信息不存在！");
            }
            return ResultBean.create(0, "success", BeanUtil.toBean(shopEntity, ShopDTO.class));
        } catch (Exception e) {
            log.error("获取商品详情失败！ e ==> {}", e);
            return null;
        }
    }

    public Shop queryWithLogicalExpire(Long id) {
        String key = RedisConstants.CACHE_SHOP_KEY + id;
        //1. 从redis中查询是否存在缓存
        String shopJson = stringRedisTemplate.opsForValue().get(key);
        //2. 判断是否存在，不存在返回null即可
        if (StrUtil.isBlank(shopJson)) {
            return null;
        }
        //3. 命中，把json反序列化为对象
        RedisData redisData = JSONUtil.toBean(shopJson, RedisData.class);
        LocalDateTime expireTime = redisData.getExpireTime();

        Shop shopEntity = JSONUtil.toBean((JSONObject) redisData.getData(), Shop.class);
        //4. 判断是否过期
        if (expireTime.isAfter(LocalDateTime.now())) {
            //4.1 如果未过期，直接返回即可
            return shopEntity;
        }

        //4.2 如果已过期，需要缓存重建
        //5. 缓存重建
        //5.1 获取互斥锁
        String lockKey = RedisConstants.LOCK_SHOP_KEY + id;
        boolean lock = tryLock(lockKey);
        if (lock) {
            //5.3 成功，开启线程，实现缓存重建
            CACHE_REBUILD_EXECUTOR.submit(() -> {
                try {
                    this.saveShopRedis(id, 20L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    unLock(lockKey);
                }
            });
        }

        //5.4 失败，直接返回结果
        return shopEntity;
    }

    public void saveShopRedis(Long id, Long expireSeconds) throws InterruptedException {
        //1. 查询店铺数据
        Shop shop = getById(id);
        Thread.sleep(200);
        //2. 封装逻辑过期时间
        RedisData redisData = new RedisData();
        redisData.setData(shop);
        redisData.setExpireTime(LocalDateTime.now().plusSeconds(expireSeconds));

        //3. 存入redis
        stringRedisTemplate.opsForValue().set(RedisConstants.CACHE_SHOP_KEY + id, JSONUtil.toJsonStr(redisData));

    }

    public Shop queryWithPassMutex(Long id) {
        String key = RedisConstants.CACHE_SHOP_KEY + id;
        //1. 从redis中查询是否存在缓存
        String shopJson = stringRedisTemplate.opsForValue().get(key);
        //2. 判断是否存在，存在返回即可
        if (StrUtil.isNotBlank(shopJson)) {
            return JSONUtil.toBean(shopJson, Shop.class);
        }
        //2.1 判断是否命中的空值
        if (shopJson != null) {
            return null;
        }
        Shop shopEntity = null;

        String lockKey = RedisConstants.LOCK_SHOP_KEY + id;
        try {
            //3.实现缓存重建
            //3.1. 获取锁
            boolean lock = tryLock(lockKey);
            //3.2 判断是否获取成功
            if (!lock) {
                //3.3 失败，则休眠重试
                Thread.sleep(10);
                queryWithPassMutex(id);
            }

            //3.4 成功，则根据id查询
            shopEntity = getById(id);
            //3.5 模拟延迟重建
            Thread.sleep(5);
            //3.6 如果为null，写入空值
            if (shopEntity == null) {
                stringRedisTemplate.opsForValue().set(key, "", RedisConstants.CACHE_NULL_TTL, TimeUnit.MINUTES);
            }
            //3.7 存在，存入redis中并设置过期时间
            stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(shopEntity), RedisConstants.CACHE_SHOP_TTL, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            //3.8 释放互斥锁
            unLock(lockKey);
        }
        //4. 返回商铺对象
        return shopEntity;
    }

    public boolean tryLock(String key) {
        Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, "0", 10, TimeUnit.SECONDS);
        return BooleanUtil.isTrue(flag);
    }

    public void unLock(String key) {
        stringRedisTemplate.delete(key);
    }


//    /**
//     * 缓存穿透解决
//     * @param id
//     * @return
//     */
//    public ShopEntity queryWithPassThrough(Long id) {
//        //1. 设置key
//        String key = RedisConstants.CACHE_SHOP_KEY + id;
//        //2. 从redis中获取该key是否存在，如果存在，直接返回，如果为空，直接返回null
//        String shopJson = stringRedisTemplate.opsForValue().get(key);
//
//        //2.1 存在
//        if (StrUtil.isNotBlank(shopJson)) {
//            return JSONUtil.toBean(shopJson, ShopEntity.class);
//        }
//
//        //判断命中的是否为空值
//        if (shopJson != null) {
//            return null;
//        }
//
//        //3. 如果不存在，从数据库中查询
//        ShopEntity shopEntity = getById(id);
//        //4. 数据库中的如果为空，则存入 redis空值，来防止缓存穿透
//        if (shopEntity == null) {
//            //4.1 存入空值
//            stringRedisTemplate.opsForValue().set(key, "", RedisConstants.CACHE_NULL_TTL, TimeUnit.MINUTES);
//            return null;
//        }
//        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(shopEntity), RedisConstants.CACHE_SHOP_TTL, TimeUnit.MINUTES);
//        //5. 返回商家对象
//        return shopEntity;
//    }

}
