package com.chen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chen.common.ResultBean;
import com.chen.dto.ShopDTO;
import com.chen.entity.Shop;

/**
 * @author whc
 * @date 2022/9/3 10:35
 */
public interface ShopService extends IService<Shop> {

    ResultBean<ShopDTO> queryById(Long id);

}
