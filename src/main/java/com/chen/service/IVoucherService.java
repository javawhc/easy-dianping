package com.chen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chen.common.ResultBean;
import com.chen.entity.Voucher;

import java.util.List;

/**
 * @author whc
 * @date 2022/9/12 13:26
 */
public interface IVoucherService extends IService<Voucher> {

    ResultBean<List<Voucher>> queryVoucherOfShop(Long shopId);

    void addSeckillVoucher(Voucher voucher);
}
