package com.chen.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.chen.entity.SeckillVoucher;

/**
 * @author whc
 * @date 2022/9/12 13:29
 */
public interface ISeckillVoucherService extends IService<SeckillVoucher> {

}
