package com.chen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chen.dto.Result;
import com.chen.entity.VoucherOrder;

/**
 * @author whc
 * @date 2022/9/12 15:23
 */
public interface IVoucherOrderService extends IService<VoucherOrder> {

    Result seckillVoucher(Long voucherId);

    void createVoucherOrder(VoucherOrder param);
}
