package com.chen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chen.entity.Shop;

/**
 * @author whc
 * @date 2022/9/3 10:33
 */

public interface ShopMapper extends BaseMapper<Shop> {


}
