package com.chen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chen.entity.VoucherOrder;

/**
 * @author whc
 * @date 2022/9/12 15:22
 */
public interface VoucherOrderMapper extends BaseMapper<VoucherOrder> {

}
