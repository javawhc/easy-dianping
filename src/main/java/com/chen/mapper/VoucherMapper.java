package com.chen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chen.entity.Voucher;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author whc
 * @date 2022/9/12 13:24
 */
public interface VoucherMapper extends BaseMapper<Voucher> {

    List<Voucher> queryVoucherOfShop(@Param("shopId") Long shopId);
}
