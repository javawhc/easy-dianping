package com.chen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chen.entity.SeckillVoucher;

/**
 * @author whc
 * @date 2022/9/12 13:31
 */
public interface SeckillVoucherMapper extends BaseMapper<SeckillVoucher> {

}
