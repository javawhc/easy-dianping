package com.chen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chen.entity.User;

/**
 * @author whc
 * @date 2022/9/12 14:50
 */
public interface UserMapper extends BaseMapper<User> {


}
