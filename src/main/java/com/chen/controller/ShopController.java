package com.chen.controller;

import com.chen.common.ResultBean;
import com.chen.dto.ShopDTO;
import com.chen.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author whc
 * @date 2022/9/3 11:06
 */
@RestController
@CrossOrigin
@RequestMapping("/shop")
public class ShopController {

    @Autowired
    private ShopService shopService;

    @GetMapping("/{id}")
    public ResultBean<ShopDTO> queryShopById(@PathVariable("id") Long id) {
        return shopService.queryById(id);
    }
}
