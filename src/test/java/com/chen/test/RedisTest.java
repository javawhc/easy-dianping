package com.chen.test;

import com.chen.service.impl.ShopServiceImpl;
import com.chen.utils.RedisIdWorker;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author whc
 * @date 2022/9/3 13:56
 */

@SpringBootTest
public class RedisTest {

    @Autowired
    private ShopServiceImpl shopService;

    @Autowired
    private RedisIdWorker redisIdWorker;

    private ExecutorService es = Executors.newFixedThreadPool(500);


    @Test
    public void testWorkerId() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(300);
        Runnable task = () -> {
            for (int i = 0; i < 100; i++) {
                long id = redisIdWorker.nextId("order");
                System.out.println("id = " + id);
            }
            latch.countDown();
        };

        long begin = System.currentTimeMillis();
        for (int i = 0; i < 300; i++) {
            es.submit(task);
        }
        latch.await();
        long end = System.currentTimeMillis();
        System.out.println("times = " + (end- begin));

    }

    @Test
    void t1(){
        System.out.println(2 << 3);
    }
//    @Test
//    void testRedis() throws InterruptedException {
////        stringRedisTemplate.opsForValue().set("user", "xiaowang");
////
////        System.out.println(stringRedisTemplate.opsForValue().get("user"));
//
//        // 做缓存预热
//        shopService.saveShopRedis(1L, 20L);
//    }

}
